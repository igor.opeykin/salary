from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from datetime import datetime, timedelta

app = FastAPI()

class AuthData(BaseModel):
    username: str
    password: str

class SalaryInfo(BaseModel):
    salary: float
    next_promotion: str

DATABASE = {
    "Ivan": {
        "password": "password123",
        "salary": 80000,
        "next_promotion": "2023-08-011"
    },
    "Sergey": {
        "password": "qwerty",
        "salary": 100000,
        "next_promotion": "2023-07-01"
    }
}

def authenticate(auth_data: AuthData):
    username = auth_data.username
    password = auth_data.password
    
    if username not in DATABASE or DATABASE[username]["password"] != password:
        raise HTTPException(status_code=401, detail="Неправильное имя пользователя или пароль")

    expiration_time = datetime.now() + timedelta(minutes=30)
    token = generate_token(username, expiration_time)
    return {"token": token, "expires_at": expiration_time}

def generate_token(username: str, expiration_time: datetime):
    return f"{username}:{expiration_time.timestamp()}"

def is_token_valid(token: str):
    try:
        username, expiration_timestamp = token.split(":")
        expiration_time = datetime.fromtimestamp(float(expiration_timestamp))
        return datetime.now() < expiration_time
    except:
        return False

@app.get("/salary")
def get_salary(token: str):
    if not is_token_valid(token):
        raise HTTPException(status_code=401, detail="Невалидный токен")
    username = token.split(":")[0]
    if username not in DATABASE:
        raise HTTPException(status_code=404, detail="Информация о зарплате не найдена")
    salary = DATABASE[username]["salary"]
    next_promotion = DATABASE[username]["next_promotion"]
    return {"salary": salary, "next_promotion": next_promotion}